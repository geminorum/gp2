if (typeof wp == 'undefined') wp = {};
wp.locale = function( translations ) {
	this.date = function(format, date) {
		if ('undefined' == typeof date) date = new Date();
		
		var jdate = JalaliDate( date ); // <---modified here
		
		var returnStr = '';
		var replace = this.replaceChars;
		var backslashCount = 0;
		for (var i = 0; i < format.length; i++) {
			var curChar = format.charAt(i);
			if (curChar == '\\') {
				backslashCount++;
				if (2 == backslashCount) {
					returnStr += '\\';
					backslashCount = 0;
				}
				continue;
			}

			if (replace[curChar] && 0 == backslashCount) {
				returnStr += replace[curChar].call(jdate); // <---modified here
			} else {
				returnStr += curChar;
			}

			if (curChar != '\\') backslashCount = 0;
		}
		//return returnStr;
		return gP2fixNumbers(returnStr, gP2Locale);
	};

	this.parseISO8601 =  function(iso8601){
		var regexp = /(\d\d\d\d)(-)?(\d\d)(-)?(\d\d)(T)?(\d\d)(:)?(\d\d)(:)?(\d\d)(\.\d+)?(Z|([+-])(\d\d)(:)?(\d\d))/;

		var matches = iso8601.match(new RegExp(regexp));
		if (!matches) return null;
		var offset = 0;

		var date = new Date();

		date.setUTCDate(1);
		date.setUTCFullYear(parseInt(matches[1],10));
		date.setUTCMonth(parseInt(matches[3],10) - 1);
		date.setUTCDate(parseInt(matches[5],10));
		date.setUTCHours(parseInt(matches[7],10));
		date.setUTCMinutes(parseInt(matches[9],10));
		date.setUTCSeconds(parseInt(matches[11],10));
		if (matches[12])
			date.setUTCMilliseconds(parseFloat(matches[12]) * 1000);
		if (matches[13] != 'Z') {
			offset = (matches[15] * 60) + parseInt(matches[17],10);
			offset *= ((matches[14] == '-') ? -1 : 1);
			date.setTime(date.getTime() - offset * 60 * 1000);
		}
		return date;
	};

	var key;
	for ( key in translations ) {
		this[ key ] = translations[ key ];
	}

	shortMonths = this.monthabbrev;
	longMonths = this.month;
	shortDays = this.weekdayabbrev;
	longDays = this.weekday;

	this.replaceChars = {

		// Day
		d: function() { return (this.getDate() < 10 ? '0' : '') + this.getDate(); },
		D: function() { return shortDays[this.getDay()]; },
		j: function() { return this.getDate(); },
		l: function() { return longDays[this.getDay()]; },
		N: function() { return this.getDay() + 1; },
		S: function() { return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th'))); },
		w: function() { return this.getDay(); },
		z: function() { return "Not Yet Supported"; },
		// Week
		W: function() { return "Not Yet Supported"; },
		// Month
		F: function() { return longMonths[this.getMonth()]; },
		m: function() { return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1); },
		M: function() { return shortMonths[this.getMonth()]; },
		n: function() { return this.getMonth() + 1; },
		t: function() { return "Not Yet Supported"; },
		// Year
		L: function() { return "Not Yet Supported"; },
		o: function() { return "Not Supported"; },
		Y: function() { return this.getFullYear(); },
		y: function() { return ('' + this.getFullYear()).substr(2); },
		// Time
		a: function() { return this.getHours() < 12 ? 'am' : 'pm'; },
		A: function() { return this.getHours() < 12 ? 'AM' : 'PM'; },
		B: function() { return "Not Yet Supported"; },
		g: function() { return this.getHours() % 12 || 12; },
		G: function() { return this.getHours(); },
		h: function() { return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12); },
		H: function() { return (this.getHours() < 10 ? '0' : '') + this.getHours(); },
		i: function() { return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes(); },
		s: function() { return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds(); },
		// Timezone
		e: function() { return "Not Yet Supported"; },
		I: function() { return "Not Supported"; },
		O: function() { return (this.getTimezoneOffset() < 0 ? '-' : '+') + (this.getTimezoneOffset() / 60 < 10 ? '0' : '') + (this.getTimezoneOffset() / 60) + '00'; },
		T: function() { return "Not Yet Supported"; },
		Z: function() { return this.getTimezoneOffset() * 60; },
		// Full Date/Time
		c: function() { return "Not Yet Supported"; },
		r: function() { return this.toString(); },
		U: function() { return this.getTime() / 1000; }
	};
};

// JalaliDate: a Date-like object wrapper for jalali.js functions
// Mehdi Jovaini <jovaini.m@nepaco.ir> (http://nepaco.ir)
function JalaliDate(p0, p1, p2) {
	var georgianDate;
	var jalaliDate;
	var isJalali = true;

	if (!p0) {
		setFullDate();
	} else if (p0 instanceof Date) { // <---modified here
		setFullDate(p0); // <---modified here
	} else if (typeof(p0) == 'boolean') {
		isJalali = p0;
		setFullDate();
	} else if (typeof(p0 == 'number')) {
		var y = parseInt(p0, 10);
		var m = parseInt(p1, 10);
		var d = parseInt(p2, 10);
		y += div(m, 12);
		m = remainder(m, 12);
		var g = jalali_to_gregorian([y, m, d]);
		setFullDate(new Date(g[0], g[1], g[2]));
	} else if (p0 instanceof Array) {
		throw new "JalaliDate(Array) is not implemented yet!";
	} else {
		setFullDate(p0);
	}

	function setFullDate(date) {
		if (date instanceof JalaliDate) {
			date = date.getGeorgianDate();
		}
		georgianDate = new Date(date);
		//console.log(georgianDate);
		if (!georgianDate || georgianDate == 'Invalid Date' || isNaN(georgianDate || !georgianDate.getDate())) {
			georgianDate = new Date();
		}
		jalaliDate = gregorian_to_jalali([
					georgianDate.getFullYear(),
					georgianDate.getMonth(),
					georgianDate.getDate()]);
		return this;
	}
	this.getGeorgianDate = function () {
		return georgianDate;
	}

	this.setFullDate = setFullDate;

	this.setDate = function (e) {
		jalaliDate[2] = e;
		var g = jalali_to_gregorian(jalaliDate);
		georgianDate = new Date(g[0], g[1], g[2]);
		jalaliDate = gregorian_to_jalali([g[0], g[1], g[2]]);
	};

	this.getFullYear = function () {
		return jalaliDate[0];
	};
	this.getMonth = function () {
		return jalaliDate[1];
	};
	this.getDate = function () {
		return jalaliDate[2];
	};
	this.toString = function () {
		return jalaliDate.join(',').toString();
	};
	this.getDay = function () {
		return georgianDate.getDay();
	};
	this.getHours = function () {
		return georgianDate.getHours();
	};
	this.getMinutes = function () {
		return georgianDate.getMinutes();
	};
	this.getSeconds = function () {
		return georgianDate.getSeconds();
	};
	this.getTime = function () {
		return georgianDate.getTime();
	};
	this.getTimeZoneOffset = function () {
		return georgianDate.getTimeZoneOffset();
	};
	this.getYear = function () {
		return JalaliDate[0] % 100;
	};

	this.setHours = function (e) {
		georgianDate.setHours(e)
	};
	this.setMinutes = function (e) {
		georgianDate.setMinutes(e)
	};
	this.setSeconds = function (e) {
		georgianDate.setSeconds(e)
	};
	this.setMilliseconds = function (e) {
		georgianDate.setMilliseconds(e)
	};
}

g_days_in_month = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
j_days_in_month = new Array(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29);

function div(a, b) {
	return Math.floor(a / b);
}

function remainder(a, b) {
	return a - div(a, b) * b;
}

function gregorian_to_jalali(g /* array containing year, month, day*/
) {
	var gy,
	gm,
	gd;
	var jy,
	jm,
	jd;
	var g_day_no,
	j_day_no;
	var j_np;

	var i;

	gy = g[0] - 1600;
	//gm = g[1]-1;
	gm = g[1];
	gd = g[2] - 1;

	g_day_no = 365 * gy + div((gy + 3), 4) - div((gy + 99), 100) + div((gy + 399), 400);
	for (i = 0; i < gm; ++i)
		g_day_no += g_days_in_month[i];
	if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))
		/* leap and after Feb */
		++g_day_no;
	g_day_no += gd;

	j_day_no = g_day_no - 79;

	j_np = div(j_day_no, 12053);
	j_day_no = remainder(j_day_no, 12053);

	jy = 979 + 33 * j_np + 4 * div(j_day_no, 1461);
	j_day_no = remainder(j_day_no, 1461);

	if (j_day_no >= 366) {
		jy += div((j_day_no - 1), 365);
		j_day_no = remainder((j_day_no - 1), 365);
	}

	for (i = 0; i < 11 && j_day_no >= j_days_in_month[i]; ++i) {
		j_day_no -= j_days_in_month[i];
	}
	//jm = i+1;
	jm = i;
	jd = j_day_no + 1;

	return new Array(jy, jm, jd);
}

function jalali_to_gregorian(j /* array containing year, month, day*/
) {
	var gy,
	gm,
	gd;
	var jy,
	jm,
	jd;
	var g_day_no,
	j_day_no;
	var leap;

	var i;

	jy = j[0] - 979;
	//jm = j[1]-1;
	jm = j[1];
	jd = j[2] - 1;

	j_day_no = 365 * jy + div(jy, 33) * 8 + div((remainder(jy, 33) + 3), 4);
	for (i = 0; i < jm; ++i)
		j_day_no += j_days_in_month[i];

	j_day_no += jd;

	g_day_no = j_day_no + 79;

	gy = 1600 + 400 * div(g_day_no, 146097);
	/* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */
	g_day_no = remainder(g_day_no, 146097);

	leap = 1;
	if (g_day_no >= 36525)
		/* 36525 = 365*100 + 100/4 */
	{
		g_day_no--;
		gy += 100 * div(g_day_no, 36524);
		/* 36524 = 365*100 + 100/4 - 100/100 */
		g_day_no = remainder(g_day_no, 36524);

		if (g_day_no >= 365)
			g_day_no++;
		else
			leap = 0;
	}

	gy += 4 * div(g_day_no, 1461);
	/* 1461 = 365*4 + 4/4 */
	g_day_no = remainder(g_day_no, 1461);

	if (g_day_no >= 366) {
		leap = 0;

		g_day_no--;
		gy += div(g_day_no, 365);
		g_day_no = remainder(g_day_no, 365);
	}

	for (i = 0; g_day_no >= g_days_in_month[i] + (i == 1 && leap); i++)
		g_day_no -= g_days_in_month[i] + (i == 1 && leap);
	//gm = i+1;
	gm = i;
	gd = g_day_no + 1;

	return new Array(gy, gm, gd);
}

function jalali_today() {
	Today = new Date();
	j = gregorian_to_jalali(new Array(
				Today.getFullYear(),
				Today.getMonth() + 1,
				Today.getDate()));
	return j[2] + "/" + j[1] + "/" + j[0];
}

//gP2fixNumbers(text, gP2Locale)
function gP2fixNumbers(text, local) {
	if (text === null){
		return null;
		};
	if ( local === 'fa_IR' ) {
		text = text.replace(/0/g, '\u06F0');
		text = text.replace(/1/g, '\u06F1');
		text = text.replace(/2/g, '\u06F2');
		text = text.replace(/3/g, '\u06F3');
		text = text.replace(/4/g, '\u06F4');
		text = text.replace(/5/g, '\u06F5');
		text = text.replace(/6/g, '\u06F6');
		text = text.replace(/7/g, '\u06F7');
		text = text.replace(/8/g, '\u06F8');
		text = text.replace(/9/g, '\u06F9');
	};
	return text;
};