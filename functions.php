<?php 
function gp2_theme_init(){
	if ( is_admin() )
		return;
	
	//add_filter( 'wp_get_attachment_metadata', 'gp2_wp_get_attachment_metadata', 10, 2 );
	
	if ( ! current_user_can( 'manage_options' ) ) 
		remove_all_actions( 'media_buttons' );
		
	if ( defined( 'GPERSIANDATE_VERSION' ) && class_exists( 'gPersianDate' ) ) {
		wp_deregister_script( 'wp-locale' );
		wp_register_script( 'wp-locale', get_stylesheet_directory_uri().'/js/wp-locale.js', array(), '20141124' ); 
		add_action( 'wp_enqueue_scripts', 'gp2_wp_enqueue_scripts', 20 );
		add_filter( 'get_post_time', 'gp2_get_post_time', 20, 3 );
	}
		
} add_action( 'init', 'gp2_theme_init', 100 );

function gari_load_textdomain_mofile( $mofile, $domain ) {
	if ( 'p2-likes' == $domain )
		return get_theme_root( 'gari' ).'/gari/languages/p2-likes-fa_IR.mo';
	return $mofile;
} if ( defined( 'P2LIKES_URL' ) ) add_filter( 'load_textdomain_mofile', 'gari_load_textdomain_mofile', 10, 2 );

function gp2_theme_setup() {
	load_theme_textdomain( 'p2', get_theme_root( 'gp2' ) . '/gp2/languages' );
	add_filter( 'p2_get_page_number', 'number_format_i18n' );

	
	if ( defined( 'P2LIKES_URL' ) ) 
		load_theme_textdomain( 'p2-likes', get_theme_root( 'gari' ) . '/gari/languages' );
	
} add_action( 'after_setup_theme', 'gp2_theme_setup' );

function gp2_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Page Sidebar', 'p2' ),
		'id' => 'side-page',
	) );
} add_filter( 'widgets_init', 'gp2_widgets_init', 20 );

function gp2_upload_mimes( $mime_types ) {
	$mime_types = array(
		'pdf' => 'application/pdf',
		'doc|docx' => 'application/msword',
	);
	return $mime_types;
} // add_filter( 'upload_mimes', 'gp2_upload_mimes' );

function gp2_post_upload_ui() {
	echo '<br />';
	_e( 'Accepted MIME types: PDF, DOC/DOCX' );
} // add_action( 'post-upload-ui', 'gp2_post_upload_ui' );


function gp2_body_class( $classes, $class ){ 
	if ( current_user_can( 'edit_others_pages' ) )
		$classes[] = 'logged-in-editor';
	else
		$classes[] = 'logged-in-noaccess';
	return $classes;
} add_filter( 'body_class', 'gp2_body_class', 10, 2 );

//if ( ! is_admin() && class_exists( 'gPersianDate' ) )

// dep!!
function gp2_wp_get_attachment_metadata( $metadata, $post_id ) {
	$metadata['width'] = number_format_i18n( $metadata['width'] );
	$metadata['height'] = number_format_i18n( $metadata['height'] );
	return $metadata;
}

function gp2_wp_enqueue_scripts() {
	remove_action( 'wp_head', array( 'P2_JS', 'locale_script_data' ), 2 );		
	add_action( 'wp_head', 'gp2_wp_head_js', 2 );		
}

function gp2_wp_head_js() {
	global $wp_locale;
	?><script type="text/javascript"> //<![CDATA[
	var wpLocale = <?php echo gPersianDate::getJSLocale( $wp_locale ); ?>;
	var gP2Locale = '<?php echo get_locale(); ?>';
//]]> </script><?php
}

function gp2_get_post_time( $time, $d, $gmt ){
	if ( 'Y-m-d\TH:i:s\Z' == $d )
		return $time;

	$post = get_post();
	if ( ! $post )
		return $time;
		
	return mysql2date( $d, ( $gmt ? $post->post_date_gmt : $post->post_date ), true );
}

